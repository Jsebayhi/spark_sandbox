import sbt._
import sbt.Keys._
import com.typesafe.sbt.SbtScalariform.autoImport._
import scalariform.formatter.preferences._

lazy val root = (project in file("."))
  .settings(
    name := "spark-sandbox",
    Global / cancelable := true, // Lets us CTRL-C partest without exiting SBT entirely
    Global / onChangedBuildSource := ReloadOnSourceChanges,// sbt auto reload if build.sbt changed.
    updateOptions := updateOptions.value.withLatestSnapshots(true),
    inThisBuild(Seq(
      organization := "sebayhi.jeremy.sandbox",
      scalaVersion := "2.12.0",
      IntegrationTest / fork := false,
      IntegrationTest / testForkedParallel := false,
      IntegrationTest / parallelExecution  := false,
      Test / fork := true,
      Test / testForkedParallel := true,
      Test / parallelExecution := true,
      concurrentRestrictions := Seq(Tags.limitAll(1)), //set to 1 to avoid that avsc schema are compiled before they are downloaded
      scalariformPreferences := scalariformPreferences.value
        .setPreference(AlignSingleLineCaseStatements, true)
        .setPreference(DoubleIndentConstructorArguments, true)
        .setPreference(DanglingCloseParenthesis, Preserve),
      scalacOptions ++= Seq(
        "-deprecation",
        "-encoding",
        "UTF-8",
        "-feature",
        "-explaintypes",
        "-language:existentials",
        "-Xfatal-warnings",
        "-Xlint"
      )
    ))
  ).aggregate(spark)

lazy val spark = projectBuilder("spark")
  .settings(SparkSandbox.settings)

def projectBuilder(directory: String): Project = {
  Project(directory, file(directory))
}
