import sbt._
import sbt.Keys._

object Dependencies {

  private val ScalaTestVersion = "3.2.0"

  def ScalaTest(scope: String) = {
    Seq(
      "org.scalatest" %% "scalatest" % ScalaTestVersion,
      "org.scalatest" %% "scalatest-flatspec" % ScalaTestVersion,
      "org.scalatest" %% "scalatest-matchers-core" % ScalaTestVersion
    ).map(_ % scope)
  }
}
