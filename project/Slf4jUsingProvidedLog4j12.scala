import sbt._
import sbt.Keys._

// The goal of all this slf4j stuff is to make sure we use one sl4j logging
// implementation. We therefore override commonly used logger and replace them with
// the slf4j dedicated override.
// This will make dependencies use slf4j without knowing it.
// Doc can be found here:
// - http://www.slf4j.org/manual.html
// - http://www.slf4j.org/legacy.html#:~:text=log4j%2Dover%2Dslf4j,simply%20by%20replacing%20the%20log4j
object Slf4jUsingProvidedLog4j12 {
  private val Slf4jVersion = "1.7.30"
  private val Log4j2ToSlf4jVersion = "2.14.1"
  private val Log4JVersion = "1.2.17"

  /**
   * Do not use with java 9+ (https://blogs.apache.org/logging/entry/moving_on_to_log4j_2)
   */
  private val dependencies: Seq[sbt.librarymanagement.ModuleID] = {
    Seq(
      // Force sl4j api version to use instead of relying on dependencies' specifications
      "org.slf4j" % "slf4j-api" % Slf4jVersion,
      // Specify log4j 1.X as the logger to use behind slf4j
      "log4j" % "log4j" % Log4JVersion % "provided",
      "org.slf4j" % "slf4j-log4j12" % Slf4jVersion % Runtime,
      // Override log4j version 2.X
      "org.apache.logging.log4j" % "log4j-to-slf4j" % Log4j2ToSlf4jVersion % Runtime,
      "org.slf4j" % "jcl-over-slf4j" % Slf4jVersion % Runtime,
      // Override Java util logging
      "org.slf4j" % "jul-to-slf4j" % Slf4jVersion % Runtime
    )
  }

  private val exclusionRules = Seq(
    // Jakarta Common Logging and it's slf4j implementation
    ExclusionRule("commons-logging", "commons-logging"),
    ExclusionRule("org.slf4j","slf4j-jcl"),
    // java util logging
    ExclusionRule("org.slf4j", "slf4j-jdk14"),
    // Log4j 2.X and it's slf4j implementation ("org.apache.logging.log4j","log4j-slf4j-impl")
    ExclusionRule("org.apache.logging.log4j"),
    // slf4j logback logger implementation
    ExclusionRule("ch.qos.logback"),
    // slf4j NOOP (/dev/null) implementation
    ExclusionRule("org.slf4j", "slf4j-nop"),
    // slf4j simple logging
    ExclusionRule("org.slf4j", "slf4j-simple"),
    // Log4j slf4j bridge which would cause a never ending log forwarding loop
    // between slf4j and log4j 1.X
    ExclusionRule("org.slf4j", "log4j-over-slf4j")
  )

  val settings = {
    Seq(
      libraryDependencies ++= dependencies,
      dependencyOverrides ++= dependencies,
      excludeDependencies ++= exclusionRules
    )
  }
}