import sbt._
import sbt.Keys._

object SparkCoreDependencies {
  private val SparkVersion = "2.4.5"

  private val dependencies: Seq[sbt.librarymanagement.ModuleID] = {
    Seq(
      "org.apache.spark" %% "spark-core" % SparkVersion % "provided" excludeAll (ExclusionRule(organization = "org.apache.hadoop")),
      "org.apache.spark" %% "spark-sql" % SparkVersion % "provided",
      "org.apache.hadoop" % "hadoop-client" % "2.9.2"
    )
  }

  private val overrides: Seq[sbt.librarymanagement.ModuleID] = {
    Seq(
      // Fixing Spark 2.4.5 trouble with jackson : https://stackoverflow.com/a/61339354
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.6.7.1",
      "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.7",
      "com.fasterxml.jackson.core" % "jackson-core" % "2.6.7"
    )
  }

  val settings = {
    Seq(
      libraryDependencies ++= dependencies,
      dependencyOverrides ++= overrides
    ) ++ Slf4jUsingProvidedLog4j12.settings
  }
}
