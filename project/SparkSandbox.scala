import Dependencies._
import sbt._
import sbt.Keys._

object SparkSandbox {
  private val dependencies: Seq[sbt.librarymanagement.ModuleID] = {
    ScalaTest("test,it")
  }

  val settings = {
    Seq(
      libraryDependencies ++= dependencies
    ) ++ SparkCoreDependencies.settings
  }
}
