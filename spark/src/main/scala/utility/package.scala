import java.time.format.{ DateTimeFormatter, DateTimeFormatterBuilder }
import java.time.temporal.ChronoField

package object utility {

  /**
   * This formatter will print as many fraction of second as available in the date.
   *
   * This is done in order to be able to do exact match with dates in data frame sql queries.
   */
  val sparkTimestampDateTimeFormatter: DateTimeFormatter = {
    new DateTimeFormatterBuilder().append(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
      .appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true)
      .toFormatter
  }

  val sparkDateFormatter: DateTimeFormatter = {
    DateTimeFormatter.ISO_DATE
  }

}
