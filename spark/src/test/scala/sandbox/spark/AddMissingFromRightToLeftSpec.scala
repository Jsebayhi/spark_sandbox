package sandbox.spark

import org.apache.spark.sql.{ Column, Dataset, Encoder }
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import org.slf4j.LoggerFactory
import sandbox.spark.support.BaseSparkSession

object AddMissingFromRightToLeft {
  import KeepRightNotInLeft.keepRightNotInLeft
  def addMissingFromRightToLeft[T](
    df1: Dataset[T],
    df2: Dataset[T],
    condition: Column
  )(implicit encoder: Encoder[T]): Dataset[T] = {
    val missing = keepRightNotInLeft(df1, df2, condition)

    df1.union(missing)
  }
}

class AddMissingFromRightToLeftSpec extends AnyFlatSpec with Matchers with BaseSparkSession {

  import AddMissingFromRightToLeft.addMissingFromRightToLeft

  val logger = LoggerFactory.getLogger(this.getClass)

  it should ("add value present on the right dataset and not on the left dataset") in {
    val expected = Seq(Data1("4", "toto"))
    testAddMissingFromRightToLeft(left = Seq(), rightNoise = Seq(), expected)
  }

  it should ("add value present on the right dataset and not on the left dataset 2") in {
    val expected = Seq(Data1("4", "toto"))
    testAddMissingFromRightToLeft(left = Seq(Data1("3", "toto")), rightNoise = Seq(Data1("3", "titi")), expected)
  }

  def testAddMissingFromRightToLeft(
    left: Seq[Data1],
    rightNoise: Seq[Data1],
    expected: Seq[Data1]): Seq[Data1] = {
    import sparkSession.sqlContext.implicits._

    val leftDs: Dataset[Data1] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(left))
    val rightDs: Dataset[Data1] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(rightNoise ++ expected))

    val res = addMissingFromRightToLeft(leftDs, rightDs, leftDs("id") === rightDs("id"))
    if (logger.isDebugEnabled) {
      res.show()
    }

    val collectedRes = res.collect().toList
    collectedRes should contain theSameElementsAs (left ++ expected)
    collectedRes
  }
}
