package sandbox.spark

import org.apache.spark.rdd.RDD
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import sandbox.spark.support.BaseSparkSession

class BasicsSpec extends AnyFlatSpec with Matchers with BaseSparkSession {

  val sc = sparkSession.sparkContext

  /**
   * unions, joins and cogroup
   */

  "union" should "work" in {
    val Input1: List[(String, String)] = List(("ID1", "content_id1_1"), ("ID2", "content_id2_1"))
    val Input2: List[(String, String)] = List(("ID2", "content_id2_2"), ("ID3", "content_id3_1"))

    val ExpectedOutput: List[(String, String)] = {
      List(
        ("ID1", "content_id1_1"),
        ("ID2", "content_id2_1"),
        ("ID2", "content_id2_2"),
        ("ID3", "content_id3_1")
      )
    }

    val input1RDD: RDD[(String, String)] = sc.parallelize(Input1)
    val input2RDD: RDD[(String, String)] = sc.parallelize(Input2)
    val result: RDD[(String, String)] = input1RDD.union(input2RDD)

    result.collect() should contain theSameElementsAs ExpectedOutput
  }

  "join" should "work" in {
    val Input1: List[(String, String)] = List(("ID1", "content_id1_1"), ("ID2", "content_id2_1"))
    val Input2: List[(String, String)] = List(("ID2", "content_id2_2"), ("ID3", "content_id3_1"))

    val ExpectedOutput: List[(String, (String, String))] = List(("ID2", ("content_id2_1", "content_id2_2")))

    val input1RDD: RDD[(String, String)] = sc.parallelize(Input1)
    val input2RDD: RDD[(String, String)] = sc.parallelize(Input2)
    val result: RDD[(String, (String, String))] = input1RDD.join(input2RDD)

    result.collect() should contain theSameElementsAs ExpectedOutput
  }

  "leftOuterJoin" should "work" in {
    val Input1: List[(String, String)] = List(("ID1", "content_id1_1"), ("ID2", "content_id2_1"))
    val Input2: List[(String, String)] = List(("ID2", "content_id2_2"), ("ID3", "content_id3_1"))

    val ExpectedOutput: List[(String, (String, Option[String]))] = {
      List(
        ("ID1", ("content_id1_1", None)),
        ("ID2", ("content_id2_1", Some("content_id2_2")))
      )
    }

    val input1RDD: RDD[(String, String)] = sc.parallelize(Input1)
    val input2RDD: RDD[(String, String)] = sc.parallelize(Input2)
    val result: RDD[(String, (String, Option[String]))] = input1RDD.leftOuterJoin(input2RDD)

    result.collect() should contain theSameElementsAs ExpectedOutput
  }

  "rightOuterJoin" should "work" in {
    val Input1: List[(String, String)] = List(("ID1", "content_id1_1"), ("ID2", "content_id2_1"))
    val Input2: List[(String, String)] = List(("ID2", "content_id2_2"), ("ID3", "content_id3_1"))

    val ExpectedOutput: List[(String, (Option[String], String))] = {
      List(("ID2", (Some("content_id2_1"), "content_id2_2")), ("ID3", (None, "content_id3_1")))
    }

    val input1RDD: RDD[(String, String)] = sc.parallelize(Input1)
    val input2RDD: RDD[(String, String)] = sc.parallelize(Input2)
    val result: RDD[(String, (Option[String], String))] = input1RDD.rightOuterJoin(input2RDD)

    result.collect() should contain theSameElementsAs ExpectedOutput
  }

  "cogroup" should "work" in {
    val Input1: List[(String, String)] = List(("ID1", "content_id1_1"), ("ID2", "content_id2_1"))
    val Input2: List[(String, String)] = List(("ID2", "content_id2_2"), ("ID3", "content_id3_1"))

    val ExpectedOutput: List[(String, (Iterable[String], Iterable[String]))] = List(
      ("ID1", (Iterable("content_id1_1"), Iterable())),
      ("ID2", (Iterable("content_id2_1"), Iterable("content_id2_2"))),
      ("ID3", (Iterable(), Iterable("content_id3_1")))
    )

    val input1RDD: RDD[(String, String)] = sc.parallelize(Input1)
    val input2RDD: RDD[(String, String)] = sc.parallelize(Input2)
    val result: RDD[(String, (Iterable[String], Iterable[String]))] = input1RDD.cogroup(input2RDD)

    result.collect() should contain theSameElementsAs ExpectedOutput
  }

  /**
   * Diverse
   */

  "reduceByKey" should "work" in {
    val Input: List[(String, String)] = {
      List(
        ("ID1", "content_id1_1"),
        ("ID2", "content_id2_1"),
        ("ID2", "content_id2_1"),
        ("ID2", "content_id2_2")
      )
    }

    val ExpectedOutput: List[(String, String)] = {
      List(
        ("ID1", "content_id1_1"),
        ("ID2", "content_id2_2")
      )
    }

    val inputRDD: RDD[(String, String)] = sc.parallelize(Input)
    val result = inputRDD.reduceByKey { case (content1, content2) => if (content1 > content2) { content1 } else { content2 } }

    result.collect() should contain theSameElementsAs ExpectedOutput
  }

  "subtractByKey" should "work" in {
    val Input1: List[(String, String)] = List(("ID1", "content_id1_1"), ("ID2", "content_id2_1"))
    val Input2: List[(String, String)] = List(("ID2", "content_id2_2"), ("ID3", "content_id3_1"))

    val ExpectedOutput: List[(String, String)] = List(("ID1", "content_id1_1"))

    val input1RDD: RDD[(String, String)] = sc.parallelize(Input1)
    val input2RDD: RDD[(String, String)] = sc.parallelize(Input2)
    val result: RDD[(String, String)] = input1RDD.subtractByKey(input2RDD)

    result.collect() should contain theSameElementsAs ExpectedOutput
  }
}

