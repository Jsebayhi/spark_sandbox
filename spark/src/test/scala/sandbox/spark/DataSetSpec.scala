package sandbox.spark

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.functions.col
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.slf4j.LoggerFactory
import sandbox.spark.support.BaseSparkSession

class DataSetSpec extends AnyFlatSpec with Matchers with BaseSparkSession {

  private val logger = LoggerFactory.getLogger(this.getClass)

  it should ("run a simple join") in {
    val d1 = Seq(Data1("1", "Smith"), Data1("2", "titi"))
    val d2 = Seq(Data2("1", "Smithy"), Data2("4", "toto"))

    val expected = Seq(Data3("1", "Smith", "Smithy"))

    import sparkSession.sqlContext.implicits._

    val df1 = sparkSession.createDataset(sparkSession.sparkContext.parallelize(d1))
    val df2 = sparkSession.createDataset(sparkSession.sparkContext.parallelize(d2))

    if (logger.isDebugEnabled) {
      df1.show()
      df2.show()
    }

    val res = df1.join(df2, col("id") === col("uuid"))
      .select(col("id"), col("stuff"), col("thing"))
      .as[Data3]

    if (logger.isDebugEnabled) {
      res.show()
    }

    res.collect() should contain theSameElementsAs (expected)
  }

  it should ("keep both left and right in a dedicated column") in {
    val d1 = Seq(Data1("1", "Smith"), Data1("2", "titi"))
    val d2 = Seq(Data2("1", "Smithy"), Data2("4", "toto"))

    val expected = Seq(WrapResult(Data1("1", "Smith"), Data2("1", "Smithy")))

    import sparkSession.sqlContext.implicits._

    val df1: Dataset[Data1] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(d1))
    val df2: Dataset[Data2] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(d2))

    if (logger.isDebugEnabled) {
      df1.show()
      df2.show()
    }

    val res: Dataset[WrapResult] = df1.joinWith(df2, $"id" === $"uuid")
      .withColumnRenamed("_1", "left")
      .withColumnRenamed("_2", "right")
      .as[WrapResult]

    if (logger.isDebugEnabled) {
      res.show()
    }
    res.collect() should contain theSameElementsAs (expected)
  }

}
