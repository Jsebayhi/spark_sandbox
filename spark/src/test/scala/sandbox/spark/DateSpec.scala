package sandbox.spark

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{ DataFrame, Dataset }
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import sandbox.spark.support.BaseSparkSession
import utility._
class DateSpec extends AnyFlatSpec with Matchers with BaseSparkSession {

  val sc = sparkSession.sparkContext

  /**
   * unions, joins and cogroup
   */

  it should "be possible to filter on a given date" in {
    val now = java.time.LocalDate.now()

    val ExpectedOutput: List[DateSpecData] = List(
      DateSpecData("ID1", java.sql.Date.valueOf(now.minusDays(1)))
    )

    val Input: List[DateSpecData] = ExpectedOutput ++ List(
      DateSpecData("ID2", java.sql.Date.valueOf(now.plusDays(1)))
    )

    import sparkSession.implicits._

    val inputRDD: RDD[DateSpecData] = sc.parallelize(Input)
    val inputDf: Dataset[DateSpecData] = inputRDD.toDS()
    val result: Dataset[DateSpecData] = inputDf
      .filter(s"to_date(date) <= to_date('${now.format(sparkDateFormatter)}')")

    result.collect() should contain theSameElementsAs ExpectedOutput
  }

}

case class DateSpecData(id: String, date: java.sql.Date)
