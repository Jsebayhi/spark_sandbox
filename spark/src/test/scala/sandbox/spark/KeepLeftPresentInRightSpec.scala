package sandbox.spark

import org.apache.spark.sql.{ Column, Dataset, Encoder }
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import org.slf4j.LoggerFactory
import sandbox.spark.support.BaseSparkSession

object KeepLeftPresentInRight {
  def keepLeftPresentInRight[T](
    left: Dataset[_],
    right: Dataset[T],
    condition: Column
  )(implicit encoder: Encoder[T]): Dataset[T] = {
    left.joinWith(right, condition, "inner")
      .select("_1.*")
      .as[T]
  }
}
class KeepLeftPresentInRightSpec extends AnyFlatSpec with Matchers with BaseSparkSession {

  import KeepLeftPresentInRight.keepLeftPresentInRight

  private val logger = LoggerFactory.getLogger(this.getClass)

  it should ("keep value present and the left and right") in {
    val d1 = Seq(Data1("1", "Smith"), Data1("2", "nan"))

    testKeepLeftPresentInRight(right = Seq(), leftNoise = Seq(), expected = d1)
  }

  it should ("discard value present on the left but not on the right") in {
    val d1 = Seq(Data1("1", "Smith"))
    testKeepLeftPresentInRight(right = Seq(), leftNoise = d1, expected = Seq())
  }

  def testKeepLeftPresentInRight(
    right: Seq[Data1],
    leftNoise: Seq[Data1],
    expected: Seq[Data1]): Seq[Data1] = {
    import sparkSession.sqlContext.implicits._

    val leftDs: Dataset[Data1] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(leftNoise ++ expected))
    val rightDs: Dataset[Data1] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(right ++ expected))

    val res = keepLeftPresentInRight(leftDs, rightDs, leftDs("id") === rightDs("id"))
    if (logger.isDebugEnabled) {
      res.show()
    }

    val collectedRes = res.collect().toList
    collectedRes should contain theSameElementsAs (expected)
    collectedRes
  }
}
