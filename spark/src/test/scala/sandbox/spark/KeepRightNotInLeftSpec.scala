package sandbox.spark

import org.apache.spark.sql.{ Column, Dataset, Encoder }
import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec
import org.slf4j.LoggerFactory
import sandbox.spark.support.BaseSparkSession

object KeepRightNotInLeft {
  def keepRightNotInLeft[T](
    df1: Dataset[_],
    df2: Dataset[T],
    condition: Column
  )(implicit encoder: Encoder[T]): Dataset[T] = {
    df1.joinWith(df2, condition, "rightouter")
      .filter("_1 is null")
      .select("_2.*")
      .as[T]
  }
}
class KeepRightNotInLeftSpec extends AnyFlatSpec with Matchers with BaseSparkSession {

  import KeepRightNotInLeft.keepRightNotInLeft

  val logger = LoggerFactory.getLogger(this.getClass)

  it should ("keep value present on the right dataset and not on the left dataset") in {
    val expected = Seq(Data1("4", "toto"))
    testKeepRightNotInLeft(left = Seq(), rightNoise = Seq(), expected)
  }

  it should ("discard value from the left dataset") in {
    val d1 = Seq(Data1("1", "Smith"), Data1("2", "nan"))

    testKeepRightNotInLeft(left = d1, rightNoise = Seq(), expected = Seq())
  }

  it should ("discard value present on the right and left dataset") in {
    val d1 = Seq(Data1("1", "Smith"))
    testKeepRightNotInLeft(left = d1, rightNoise = d1, expected = Seq())
  }

  def testKeepRightNotInLeft(
    left: Seq[Data1],
    rightNoise: Seq[Data1],
    expected: Seq[Data1]): Seq[Data1] = {
    import sparkSession.sqlContext.implicits._

    val leftDs: Dataset[Data1] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(left))
    val rightDs: Dataset[Data1] = sparkSession.createDataset(sparkSession.sparkContext.parallelize(rightNoise ++ expected))

    val res = keepRightNotInLeft(leftDs, rightDs, leftDs("id") === rightDs("id"))
    if (logger.isDebugEnabled) {
      res.show()
    }

    val collectedRes = res.collect().toList
    collectedRes should contain theSameElementsAs (expected)
    collectedRes
  }
}

case class Data1(id: String, stuff: String)
case class Data2(uuid: String, thing: String)
case class Data3(id: String, stuff: String, thing: String)
private[spark] case class WrapResult(left: Data1, right: Data2)
