package sandbox.spark.support

import org.scalatest.Suite

trait BaseSparkSession {
  this: Suite =>

  import org.apache.spark.sql.SparkSession

  protected val sparkSessionBuilder: SparkSession.Builder = SparkSession
    .builder()
    .appName("spark-scala-sandbox")
    .master("local[2]")

  val sparkSession: SparkSession = sparkSessionBuilder.getOrCreate()

}
